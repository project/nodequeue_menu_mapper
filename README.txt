
This module was built to allow synchronizing nodequeue's and menus. If you've got a site with list pages where a
view is controlled by a nodequeue, and there is to be an identical menu somewhere else... this will bridge the gap! If
you rearrange the nodequeue, it updates the correspodning menu. If you update the menu, it updates the corresponding
nodequeue.

This is all managed via some hooks, a few custom functions, a few janks because drupal menu is funky, and a side
of scret sauce!

Full documentation to come as this leaves dev!
